package com.assembly;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.production.Unit;

@Component
public class Outlander implements Car {

	@Autowired
	@Qualifier("engine")
	private Unit mitsubishEngine;
	
	@Autowired
	@Qualifier("tyre")
	private Unit mitsubishiTyre;

	@Value("${outlander.odo}")
	private String odometer;
	
	@PostConstruct
	public void headsUp() {
		System.out.println("This is @PostConstruct Hook method from Outlander: My OutLander is limited edition: SPORTS");
	}
	
	@PreDestroy
	public void goodBye() {
		System.out.println("This is @PreDestroy Hook method from Outlander: BYE FROM OUTLANDER: outlander is actually cute");
	}
	@Override
	public String getCarName() {
		// TODO Auto-generated method stub
		return "Mitsubishi OutLander Sports";
	}

	@Override
	public String getEngineSpecs() {
		// TODO Auto-generated method stub
		return mitsubishEngine.getUnitDetail();
	}

	@Override
	public String getTyreSpecs() {
		// TODO Auto-generated method stub
		return mitsubishiTyre.getUnitDetail();
	}

	@Override
	public String getOdoReading() {
		// TODO Auto-generated method stub
		return getOdometer();
	}

	public String getOdometer() {
		return odometer;
	}

	public void setOdometer(String odometer) {
		this.odometer = odometer;
	}

}
