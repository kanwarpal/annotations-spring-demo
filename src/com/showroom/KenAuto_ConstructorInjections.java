package com.showroom;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.assembly.Car;
import com.assembly.Cressida;

public class KenAuto_ConstructorInjections {

	public static void main(String[] args) {
		
		System.out.println("Welcome to: KenAuto, Punjab.");
		System.out.println("..We use constructor-injections for bodyparts..");
		//load the spring configuration context, that is loading the Spring IoC container.
		ClassPathXmlApplicationContext resource = new ClassPathXmlApplicationContext("spring-configuration.xml");
		
		//retrieve the bean from the IoC container.
		Car eightyTwoSeventyOne = resource.getBean("cressida", Cressida.class);
		
		//CaLL methods.
		System.out.println(eightyTwoSeventyOne.getCarName());
		System.out.println("Engine: "+eightyTwoSeventyOne.getEngineSpecs());
		System.out.println("Tyres Dimensions: "+eightyTwoSeventyOne.getTyreSpecs());
		System.out.println("ODOMETER-READING: "+ eightyTwoSeventyOne.getOdoReading());
		
		//close the context.
		resource.close();
	}

}
