package com.assembly;

public interface Car {
	
	String getCarName();
	
	String getEngineSpecs();
	
	String getTyreSpecs();
	
	String getOdoReading();
	
}
