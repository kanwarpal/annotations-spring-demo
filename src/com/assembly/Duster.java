package com.assembly;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.production.Unit;

@Component
public class Duster implements Car {

	private Unit engine;
	private Unit tyre;
	
	@Value("${duster.odo}")
	private String odometer;
	
	@Autowired
	public void setEngine(Unit engine) {
		this.engine = engine;
	}
	
	@PostConstruct
	public void headsUp() {
		System.out.println("This is @PostConstruct Hook Method from Duster.java : My Duster is 4 X 4, LED tail lights");
	}
	
	@PreDestroy
	public void byeDuster() {
		System.out.println("THis is @PostDestroy Hook Method from Duster.java: BYE from DUSTER, Enjoy Your civil tank");
	}
	
	@Autowired
	public void setTyre(Unit tyre) {
		this.tyre = tyre;
	}
	
	@Override
	public String getCarName() {
		// TODO Auto-generated method stub
		return "Renault Duster";
	}

	@Override
	public String getEngineSpecs() {
		// TODO Auto-generated method stub
		return engine.getUnitDetail();
	}

	@Override
	public String getTyreSpecs() {
		// TODO Auto-generated method stub
		return tyre.getUnitDetail();
	}

	@Override
	public String getOdoReading() {
		// TODO Auto-generated method stub
		return getOdometer();
	}

	public String getOdometer() {
		return odometer;
	}

	public void setOdometer(String odometer) {
		this.odometer = odometer;
	}

}
