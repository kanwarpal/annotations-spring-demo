package com.showroom;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.assembly.Car;
import com.assembly.Cressida;
import com.springConfig.SpringConfiguration;

public class LatestAuto_Non_XML {
	
	public static void main(String[] args) {
		System.out.println("Welcome to: LatestAuto, Punjab.");
		System.out.println("..We use No XML file at all for bodyparts..");
		
		//load the spring container configuration file
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
	
		//retrieve the bean from container
		Car eightyTwoSeventyOne = context.getBean("cressida", Cressida.class);
		//CaLL methods.
		System.out.println(eightyTwoSeventyOne.getCarName());
				System.out.println("Engine: "+eightyTwoSeventyOne.getEngineSpecs());
				System.out.println("Tyres Dimensions: "+eightyTwoSeventyOne.getTyreSpecs());
				System.out.println("ODOMETER-READING: "+ eightyTwoSeventyOne.getOdoReading());
				
				//close the context.
				context.close();
		}
}