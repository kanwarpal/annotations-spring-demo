package com.showroom;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.assembly.Car;
import com.assembly.Duster;

public class BlockAuto_SetterInjections {

	public static void main(String[] args) {
	
		System.out.println("Welcome to: BlockAuto, Punjab.");
		System.out.println("..We use setter-injections for bodyparts..");
		//load the spring configuration context, that is loading the Spring IoC container.
		ClassPathXmlApplicationContext resource = new ClassPathXmlApplicationContext("spring-configuration.xml");
		
		//retrieve the bean from the IoC container.
		Car fourtyfourHundred = resource.getBean("duster", Duster.class);
		
		//CaLL methods.
		System.out.println(fourtyfourHundred.getCarName());
		System.out.println("Engine: "+fourtyfourHundred.getEngineSpecs());
		System.out.println("Tyres Dimensions: "+fourtyfourHundred.getTyreSpecs());
		System.out.println("ODOMETER READING: "+ fourtyfourHundred.getOdoReading());
		
		//close the context.
		resource.close();
	}
		
	}


