package com.assembly;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.production.Unit;

@Component
public class Cressida implements Car {

	private Unit engine;
	private Unit tyre;
	
	//Literal Injection from Properties file:
	@Value("${cressida.odo}")
	private String odometer;

	//Using @Qualifier inside Constructor-arguments. 
	@Autowired
	public Cressida(@Qualifier("engine") Unit engine1, @Qualifier("tyre") Unit tyre1){
		this.engine = engine1;
		this.tyre = tyre1;
	}
	
	@Override
	public String getCarName() {
		
		return "Toyota Cressida";
	}

	@Override
	public String getEngineSpecs() {
		
		return engine.getUnitDetail();
	}

	@Override
	public String getTyreSpecs() {
		
		return tyre.getUnitDetail();
	}

	@Override
	public String getOdoReading() {
		// TODO Auto-generated method stub
		return getOdometer();
	}

	public String getOdometer() {
		return odometer;
	}

	public void setOdometer(String odometer) {
		this.odometer = odometer;
	}


}
